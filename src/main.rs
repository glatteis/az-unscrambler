pub mod unscrambler;
pub mod site_getter;
pub mod telegram;
use clap::{App, Arg};
use std::fs::read_to_string;
use std::fs::File;
use std::io::{prelude::*, BufReader};
use tokio;

#[tokio::main]
async fn main() {
    let matches = App::new("az-unscrambler")
        .version("0.2")
        .author("Linus Heck <linus.heck@rwth-aachen.de>")
        .arg(
            Arg::with_name("method")
                .short("m")
                .possible_values(&["train", "unscramble", "generate", "bot"])
                .default_value("unscramble")
                .help("What to do")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("file")
                .short("f")
                .help("The file where input text is stored.")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("url")
                .short("u")
                .help("URL of an article to unscramble.")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("TEXT")
                .help("Text to unscramble")
                .index(1)
                .required(false),
        )
        .get_matches();
    let method = matches.value_of("method").unwrap();
    match method {
        "train" => {
            let mut unscrambler = unscrambler::Unscrambler::new().await.unwrap();

            let file = File::open("dataset/words.txt").unwrap();
            let reader = BufReader::new(file);

            let mut counter = 0;
            let mut accumulated_string = Vec::new();
            println!("Training dictionary...");
            for line in reader.lines() {
                accumulated_string.push(line.unwrap());
                if counter == 0 {
                    println!("{}, etc", accumulated_string[0]);
                }
                counter += 1;
                if counter == 10000 {
                    unscrambler.train_dictionary(&accumulated_string).await.unwrap();
                    counter = 0;
                    accumulated_string = Vec::new();
                }
            }

            let file = File::open("dataset/training.txt").unwrap();
            let reader = BufReader::new(file);

            for line in reader.lines() {
                unscrambler.train_text(&line.unwrap()).await.unwrap();
            }
        }
        "unscramble" => {
            let file_name = matches.value_of("file");
            let url = matches.value_of("url");
            let text: String;
            if file_name.is_some() {
                text = read_to_string(file_name.unwrap()).unwrap();
            } else if url.is_some() {
                text = site_getter::get_text(url.unwrap()).await.unwrap().join("\n");
            } else {
                text = String::from(matches.value_of("TEXT").unwrap());
            }
            println!("{}", text);
            let mut unscrambler = unscrambler::Unscrambler::new().await.unwrap();
            println!("{}", unscrambler.unscramble_text(&text).await.unwrap());
        }
        "bot" => {
            telegram::stream().await.unwrap();
        }
        &_ => {}
    }
}
