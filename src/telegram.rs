use eyre::{Result, WrapErr, eyre};
use teloxide::prelude::*;
use url::Url;
use crate::unscrambler::Unscrambler;
use crate::site_getter;

pub async fn stream() -> Result<()> {
    let bot = Bot::from_env().auto_send();

    teloxide::repl(bot, |message| async move {
        let result = process(&message).await;
        if result.is_err() {
            println!("{:?}", result);
            message.answer("Encountered an error.").await?;
        }
        respond(())
    }).await;
    Ok(())
}

async fn process(message: &UpdateWithCx<AutoSend<Bot>, Message>) -> Result<()> {
    let text = message.update.text();
    // These things are there because the bot should not throw errors when posting other links or
    // discussing in chat.
    if text.is_none() {
        println!("(Message: Text empty)");
        return Ok(())
    }
    let text = text.unwrap();
    let result = Url::parse(text);
    if result.is_err() {
        println!("(Message: URL invalid)");
        return Ok(())
    }

    let text_vec = site_getter::get_text(text).await;
    if text_vec.is_err() {
        println!("(Message: Site getter error {:?})", text_vec);
        return Ok(())
    }
    let mut text_vec = text_vec.unwrap();
    let first_paragraph = text_vec.remove(0);
    let sent_message = message.answer("Sortiere diesen Diamanten des Qualitätsjournalismus").await?;
    let tasks: Vec<_> = text_vec.into_iter().map(|paragraph| {
        tokio::spawn(async move {
            let mut unscrambler = Unscrambler::new().await.map_err(|e| eyre!(e)).wrap_err("Couldn't create unscrambler")?;
            unscrambler.unscramble_text(&paragraph).await.map_err(|e| eyre!(e)).wrap_err("Couldn't unscramble text")
        })
    }).collect();
    let mut unscrambled_texts = vec![Ok(first_paragraph.to_string())];
    for task in tasks {
        unscrambled_texts.push(task.await.map_err(|e| eyre!(e)).wrap_err("Couldn't finish task")?);
    }
    message.requester.delete_message(sent_message.chat_id(), sent_message.id).await.map_err(|e| eyre!(e)).wrap_err("Couldn't delete message")?;

    let mut final_text_buckets = vec![String::from("")];
    for text in unscrambled_texts {
        let text = text?;
        let new_string = final_text_buckets[final_text_buckets.len() - 1].to_string() + "\n" + &text;
        if new_string.len() >= 4096 {
            final_text_buckets.push(text);
        } else {
            let last_index = final_text_buckets.len() - 1;
            final_text_buckets[last_index] = new_string;
        }
    }

    if final_text_buckets[0].replace("\n", "").is_empty() {
        return Ok(())
    }

    for final_text in final_text_buckets {
        message.answer(final_text).await.map_err(|e| eyre!(e)).wrap_err("Couldn't send message")?;
    }
    Ok(())
}
