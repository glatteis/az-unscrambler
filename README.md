# The challenge

Some of my fellow students noticed that the local newspapers Aachener Zeitung and Aachener
Nachrichten have a paywall, but do send the article in a scrambled format if you look at the site
and are not logged in. The words and letters are the same, but they are scrambled, like this:

> sDa eTma nvo MEUS etabu rfü ieen eTreesthi ni meein tuampkerrS ide nuene ronseenS ni edn aMnße ovn ageneRl nud teewintcekl ide daspeens tfweSrao uzm snAselue rde .eDatn

The challenge was to unscramble the original text from this information. For instance, this text
becomes

> Das Team von muse baute für eine Testreihe in einem Supermarkt die neuen Sensoren in den Maßen von Regalen und entwickelte die passende Software zum Auslesen der Daten.

There is one only mistake in here - the team is from EMSU, not muse.

# How do I use it?

First, you need to prove that you are really a student from Aachen and you just want to try it out
for research purposes. This is not meant for pirating newspapers! Please answer this question: What
is the surname of the person that is the namesake for a certain gate that you may not walk through,
otherwise it means bad luck for your study? This surname in small ASCII letters is the gpg key for the
file `dataset/training.txt.gpg`. Put the result into `dataset/training.txt`.

Now, setup a local postgres database with no password and create the table `unscrambler`.
Build the project with `cargo build --release`. Now you can train the unscrambler with 
`./target/release/az-unscrambler -m train` and unscramble after by passing it a string or an url
(see help). There is also a WIP Telegram bot functionality you can activate with `-m bot`.

# How does it work?

It uses a Markov Chain that stores which words frequently occur after each other, and their sorted
letters. Given a sentence, the program first calculates a confidence on every word. Then the word
with highest confidence is replaced to the correct word. Afterwards, the confidence of the
surrounding words is updated. This is repeated until the entire sentence is unscrambled.

# This is not meant for pirating newspapers.

Local newspapers are dying and need your help. Please support local newspapers and get a
subscription. This is just a language experiment - I needed to find out if I could write a good
unscrambler based on what AZ/AN gives me.
